<?php
/**
 * @Copyright (C) 2015-2017, RB Consulting, Inc. All Rights Reserved
 * 
 * @author Rob Broadhead
 * 
 * PDO Database class
 */
class PDO_Database extends PDO {

  private $db;

  function __construct () {
    parent::__construct('mysql:host=127.0.0.1;dbname=warehouse;charset=utf8', 'demoroot', 'w@r3h0us3');
    $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
  }

  // Run SQL statement (SELECT) and return all rows in an array
  public function fetch_all($sql, $parameters = array()) {
    $pdo_stmt = $this->prepare_and_execute($sql, $parameters);
    $rows = $pdo_stmt->fetchAll();
    $pdo_stmt->closeCursor();
    unset($pdo_stmt);
    return($rows);
  }

  // Run SQL statement (SELECT) and return one row in an array
  public function fetch_one($sql, $parameters=array()) {
    $rows = $this->fetch_all($sql, $parameters);
    if ($rows != false) {
      return($rows[0]);
    } else {
      return ""; 
    }
  }

  // Execute an SQL statement (INSERT, UPDATE)
  public function execute($sql, $parameters) {
    $pdo_stmt = $this->prepare_and_execute($sql, $parameters);
    if (empty($result)) {
      return 0;
    } else {
      return($pdo_stmt->rowCount());
    }
  }

  // Prepare and execute SQL statement
  private function prepare_and_execute($sql, $parameters=array()) {
    try {
      $pdo_stmt = $this->prepare($sql);
      $pdo_stmt->execute($parameters);
      return($pdo_stmt);
    }
    catch (PDOException $e) {
      print $e->getCode() . " : " . $sql ."\n";
      // Handle the exception
    }
  }
}