<?php
include_once('init.php');
include_once('PDO_Database.class.php');

// Setup a global connection
$PDO = new PDO_Database();

// Load the data classes up front
include_once('classes/storeclass.php');
include_once('classes/productclass.php');

?>