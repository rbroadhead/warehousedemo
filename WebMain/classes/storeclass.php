<?php
/**
 * @Copyright (C) 2017, RB Consulting, Inc. All Rights Reserved
 *
 * @author Rob Broadhead
 *
 * Store Class
 */
// Class for keeping track of store locations
class StoreData {
  private $id = '0';
  private $code = '';
  private $name = '';
  private $zip = '';

  public function getId() {
    return $this -> id;
  }

  public function setId($value) {
    $this -> id = $value;
  }

  public function getName() {
    return $this -> name;
  }

  public function setName($value) {
    $this -> name = $value;
  }

  public function getCode() {
    return $this -> code;
  }

  public function setCode($value) {
    $this -> code = $value;
  }

  public function getZip() {
    return $this -> zip;
  }

  public function setZip($value) {
    $this -> zip = $value;
  }

  public function toString() {
    $retVal = $this->id . "|";
    $retVal .= $this->name . "|";
    $retVal .= $this->zip . "|";
    $retVal .= $this->code;
    return $retVal;
    
  }

  public static function getZipLocations($zip) {
    global $PDO;
    $retVal = array();

    $sql_params = array('zipcode' => $zip);
    $sql = "SELECT id,name,code,zipcode FROM Store ";
    $sql .= " where 0=:zipcode OR zipcode=:zipcode order by name";
    $result = $PDO->fetch_all($sql, $sql_params);
    $idx = 0;
    foreach ($result as $row) {
      $curObj = new StoreData();
      $curObj -> setId($row['id']);
      $curObj -> setName($row['name']);
      $curObj -> setCode($row['code']);
      $curObj -> setZip($row['zipcode']);
      $retVal[$idx++] = $curObj;
    }
    return $retVal;
  }

  
  public static function getLocationsSelectByZip($zip,$theVal) {
    $retVal = '<select id="storeelect" name="storeselect">';
    $result = StoreData::getZipLocations($zip);

    foreach ($result as $row) {
      $select = "";
      if ($row -> getId() == $theVal) {
        $select = "selected";
      }
      $retVal .= '<option value="' . $row -> getId() . '" '.$select.'>' . $row -> getName() . '</option>\n';
    }
	if ($retVal == '<select id="storeselect" name="storeselect">') {
		$retVal .= '<option value="0">No Stores Available At That Zip</option'; 
	}

    return $retVal . "</select>\n";
  }

  public function handleEmpty($srcString) {
    if ($srcString == "") {
      $srcString = "Not Entered";
    }

    return $srcString;
  }
}
?>