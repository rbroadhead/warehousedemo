<?php
/**
 * @Copyright (C) 2017, RB Consulting, Inc. All Rights Reserved
 *
 * @author Rob Broadhead
 *
 * Store Class
 */
// Class for keeping track of product locations
class ProductData {
  private $id = '0';
  private $code = '';
  private $name = '';
  private $storeId = '';
  private $row = '';
  private $section = '';
  private $shelf = '';
  private $bin = '';

  public function getId() {
    return $this -> id;
  }

  public function setId($value) {
    $this -> id = $value;
  }

  public function getSection() {
    return $this -> section;
  }

  public function setSection($value) {
    $this -> section = $value;
  }

  public function getName() {
    return $this -> name;
  }

  public function setName($value) {
    $this -> name = $value;
  }

  public function getCode() {
    return $this -> code;
  }

  public function setCode($value) {
    $this -> code = $value;
  }

  public function getStore() {
    return $this -> storeId;
  }

  public function setStore($value) {
    $this -> storeId = $value;
  }

  public function getRow() {
    return $this -> row;
  }

  public function setRow($value) {
    $this -> row = $value;
  }

  public function getShelf() {
    return $this -> shelf;
  }

  public function setShelf($value) {
    $this -> shelf = $value;
  }

  public function getBin() {
    return $this -> bin;
  }

  public function setBin($value) {
    $this -> bin = $value;
  }

  public function toString() {
    $retVal = $this->id . "|";
    $retVal .= $this->name . "|";
    $retVal .= $this->row . "|";
    $retVal .= $this->shelf . "|";
    $retVal .= $this->section . "|";
    $retVal .= $this->bin . "|";
    $retVal .= $this->storeId . "|";
    $retVal .= $this->code;
    return $retVal;
    
  }

  public static function getStoreItems($store) {
    global $PDO;
    $retVal = array();

    $sql_params = array('storeid' => $store);
    $sql = "SELECT * FROM Product where storeId=:storeid order by name";
    $result = $PDO->fetch_all($sql, $sql_params);
    $idx = 0;
    foreach ($result as $row) {
      $curObj = new ProductData();
      $curObj -> setId($row['id']);
      $curObj -> setName($row['name']);
      $curObj -> setCode($row['code']);
      $curObj -> setStore($row['storeId']);
      $curObj -> setRow($row['row']);
      $curObj -> setShelf($row['shelf']);
      $curObj -> setSection($row['section']);
      $curObj -> setBin($row['bin']);
      $retVal[$idx++] = $curObj;
    }
    return $retVal;
  }

  
  public static function getProductSelectByStore($store,$theVal) {
    $retVal = '<select id="productselect" name="productselect">';
    $result = ProductData::getStoreItems($store);

    foreach ($result as $row) {
      $select = "";
      if ($row -> getId() == $theVal) {
        $select = "selected";
      }
      $retVal .= '<option value="' . $row -> getId() . '" '.$select.'>' . $row -> getName() . '</option>\n';
    }
	if ($retVal == '<select id="productselect" name="productselect">') {
		$retVal .= '<option value="0">No Products Available At That Store</option'; 
	}

    return $retVal . "</select>\n";
  }

  public function handleEmpty($srcString) {
    if ($srcString == "") {
      $srcString = "Not Entered";
    }

    return $srcString;
  }
}
?>