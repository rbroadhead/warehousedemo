<?php 
include("include/settings.php");

if (isset($_POST['zip'])) {
  $zipVal = $_POST['zip'];
} else {
  header("Location: index.php?msg=1");	
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Demo</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/blog.css" rel="stylesheet">
    <link href="css/basic.css" rel="stylesheet">
  </head>
  <body>
	<div class="container">
      <div class="blog-header">
        <div class="jumbotron2">
			<h2 class="blog-post-title">Item Locator Demo</h2><br/>
        </div>
      </div>
		<div class="row ">
			<div class="col-md-6 blog-main">
				<h4>Please Select a Store:</h4>
				<form id="zipForm" action="productList.php" method="post">
					<?php echo StoreData::getLocationsSelectByZip($zipVal,0) ?>
					<input type="submit" value="Show Products" />
				</form>
			</div>
		</div>
	</div>
	<br/>
  </body>
</html>