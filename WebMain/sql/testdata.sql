-- Test Stores
INSERT INTO Store (id, code, name, zipcode) VALUES (1, 'TEST1', 'Test Store 1', '37064');
INSERT INTO Store (id, code, name, zipcode) VALUES (2, 'TEST2', 'Test Store 2', '37064');
INSERT INTO Store (id, code, name, zipcode) VALUES (3, 'TEST3', 'Test Store 3', '37064');
INSERT INTO Store (id, code, name, zipcode) VALUES (4, 'TEST4', 'Test Store 4', '37001');
INSERT INTO Store (id, code, name, zipcode) VALUES (5, 'TEST5', 'Test Store 5', '37001');
INSERT INTO Store (id, code, name, zipcode) VALUES (6, 'TEST6', 'Test Store 6', '37032');
INSERT INTO Store (id, code, name, zipcode) VALUES (7, 'TEST7', 'Test Store 7', '37032');
INSERT INTO Store (id, code, name, zipcode) VALUES (8, 'TEST8', 'Test Store 8', '37034');
INSERT INTO Store (id, code, name, zipcode) VALUES (9, 'TEST9', 'Test Store 9', '37035');
INSERT INTO Store (id, code, name, zipcode) VALUES (10, 'TEST10', 'Test Store 10', '37036');

-- Test Products
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ('TP1', 'Seafood Cocktail Sauce', 1, 3, 'A', 'A001 - Crosse & Blackwell', 'Condiments');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ('TP2', 'Cocktail Sauce', 1, 3, 'A', 'A002 - House Autry', 'Condiments');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP3', 'Test Product 3', 1, 1, 'A', 2, 'Condiments');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP4', 'Test Product 4', 1, 1, 'A', 2, 'Condiments');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP5', 'Test Product 5', 1, 1, 'A', 3, 'Condiments');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP6', 'Test Product 6', 1, 1, 'A', 3, 'Condiments');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP7', 'Test Product 7', 1, 1, 'B', 1, 'Beverages');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP8', 'Test Product 8', 1, 1, 'B', 2, 'Beverages');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP9', 'Test Product 9', 1, 1, 'B', 1, 'Beverages');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP10', 'Test Product 10', 1, 2, 'A', 1, 'Beverages');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP11', 'Test Product 11', 1, 2, 'A', 1, 'Beverages');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP12', 'Test Product 12', 1, 2, 'A', 2, 'Canned Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP13', 'Test Product 13', 1, 2, 'A', 2, 'Canned Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP14', 'Test Product 14', 1, 2, 'A', 3, 'Canned Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP15', 'Test Product 15', 1, 2, 'B', 1, 'Canned Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP16', 'Test Product 16', 1, 2, 'B', 2, 'Canned Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP17', 'Test Product 17', 1, 2, 'B', 3, 'Dry Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP18', 'Test Product 18', 1, 2, 'C', 1, 'Dry Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP19', 'Test Product 19', 1, 2, 'C', 2, 'Dry Goods');
INSERT INTO Product ( code, name, storeId, row, shelf, bin, section) VALUES ( 'TP20', 'Test Product 20', 1, 2, 'C', 3, 'Dry Goods');

insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 2, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 3, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 4, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 5, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 6, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 7, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 8, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 9, row, shelf, bin, section from Product where storeId=1;
insert into Product (code, name, storeId, row, shelf, bin, section) select  code, name, 10, row, shelf, bin, section from Product where storeId=1;