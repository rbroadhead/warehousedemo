CREATE DATABASE warehouse;
use warehouse;

--CREATE USER 'demoroot'@'localhost' IDENTIFIED BY 'w@r3h0us3';
--GRANT ALL PRIVILEGES ON warehouse . * TO 'demoroot'@'localhost';
--FLUSH PRIVILEGES;

CREATE TABLE
    Store
    (
        id bigint NOT NULL AUTO_INCREMENT,
        code VARCHAR(10) NOT NULL,
        name VARCHAR(255) NOT NULL,
        zipcode VARCHAR(5),
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
CREATE TABLE
    Product
    (
        id bigint NOT NULL AUTO_INCREMENT,
        code VARCHAR(10) NOT NULL,
        name VARCHAR(255) NOT NULL,
        storeId bigint not null,
        row  int not null,
        shelf varchar(30) not null,
        section varchar(30) not null,
        bin varchar(30) not null,
        PRIMARY KEY (id),
        FOREIGN KEY (storeId) REFERENCES Store (id)
    )
ENGINE=InnoDB DEFAULT CHARSET=latin1;
