<?php
include ("include/settings.php");

if (isset($_POST['storeselect'])) {
	$store = $_POST['storeselect'];
} else {
	header("Location: index.php?msg=2");
}

$prodList = ProductData::getStoreItems($store);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Demo</title>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
		<script>
			var priorId = 0;
			function displayProduct() {
				id = document.getElementById("productselect").value;
				if (priorId != 0) {
					document.getElementById("prodDisplay" + priorId).style.display = 'none';
				}
				priorId = id;
				document.getElementById("prodDisplay" + id).style.display = 'block';
			}
		</script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Custom styles for this template -->
		<link href="css/blog.css" rel="stylesheet">
		<link href="css/basic.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<div class="blog-header">
				<div class="jumbotron2">
					<h2 class="blog-post-title">Item Locator Demo</h2>
					<br/>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-6 blog-main">
					<h4>Please Select a Product:</h4>
					<form id="zipForm" action="javascript:displayProduct();" method="post">
						<?php echo ProductData::getProductSelectByStore($store,0)
						?>
						<input type="submit" value="Display Product Location" />
					</form>
				</div>
			</div>
			<br/>
			<br/>
			<div class="row ">
				<div class="col-md-6">
					<?php
					foreach ($prodList as $product) {
						echo '<div id="prodDisplay' . $product -> getId() . '" style="display:none;"><strong>You can find your product here:</strong><br/>Row: ' . $product -> getRow() . '<br/>Shelf: ' . $product -> getShelf() . '<br/>Section: ' . $product->getSection() . '<br/>Bin: ' . $product -> getBin() . '</div>';
					}
					?>
				</div>
			</div>
		</div>
	</body>
</html>